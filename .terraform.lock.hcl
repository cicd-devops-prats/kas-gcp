# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/gitlabhq/gitlab" {
  version = "3.6.0"
  hashes = [
    "h1:icNPivGM5+Rilvii04ygDEycpSGRzQ4oH+4oXKfhd8M=",
    "zh:08a915f21564737090eea015f47d7dd5c2d1a5f63b49211937534f643f47b8ca",
    "zh:145527bed51ec8070a85e967395fcd9aa32c97fbf2cf42747397cef6f1388213",
    "zh:1e3c8c26af5076b9067ac9f6f71b188c25838db42e8d6c11128322d6c4655982",
    "zh:5a795890e78121f7b8d8a3ae6fdc6a6f69083465b6a2073c9c0f0564dad4f7ec",
    "zh:765219c271e8af16fd436cf88e32079379b035d92bca0df556dee9adf9c9152f",
    "zh:7804d99ba61f74db1a4a911d181e7f8cee62c41658fe424cfa28362d6bbe8784",
    "zh:85e51fd6402de3e842e75ea02dc79cc3903fd030bd3d312bfcb78a8a61b8dfaa",
    "zh:8b13a75dcd932dc43616d058eeae60fada70bc93b0deb7152a4aba55d365e5b0",
    "zh:8d80014cc5acf9c4c64cd73e8f15b418b52cc54e112b1ebb49fdefa2ad8e13eb",
    "zh:b25129758f8aa6835dd22101f945abed6430d457659d20d2638eff27ca71bddd",
    "zh:b35e089343a6185db0196273517fcfe98fbd13d6ac1901b49d1d24b2b39b2ff1",
    "zh:cd928bc0e5d42fb966fd583cb8c46169cf80d35a57859db949d0c0b802fef35d",
    "zh:f8af5be58d41afcda522926e3768728fe412a87dbacca7ecb02341b854bf798f",
  ]
}

provider "registry.terraform.io/hashicorp/google" {
  version = "3.64.0"
  hashes = [
    "h1:9zHVjNsfxu+4PZDf1cQXUa2asgaTXqo/t62sZbOHALg=",
    "zh:0d96631da649a04ed1e9f44cbe24ff9d2cf8b7b0b478318bf3fa093a5118763c",
    "zh:1b549a0f7905ccb5fd519437b13a1d5405fed8d21810e750cfc7e58f3d16f790",
    "zh:1da4caa9dbec76c256d28b03259b69be758f2f31f2349c0bf48dacbe747512af",
    "zh:49a4e1e8feb1ad9b3dfce8788c747b749e183330179a134732c9651d0eca53fd",
    "zh:570d56546a7ae79e645c3ad5211badc45fa5cc64a9e41ef908c05b8bfe8c8443",
    "zh:7864b3fdcc5d143d89b6f7129ffb6ccddb665fa991b3fad684877bb191678fef",
    "zh:9c1dda1a45ee4aeb313fd700aca2425366f0b8baa11df1628cccf91fdc36fc43",
    "zh:c0e0ea9223bd47b3a5e963117052ae64d8130c2a20725244f20a80077daf1e6b",
    "zh:f1a12bace2b4f1d3092baf8e2aeefcb0e785b33bd4890edf069149dec2b07ebe",
    "zh:f221619884ed97e37c42febafd293d39747401917498bb24cdf051e9474f19b7",
    "zh:f22c99eb7132bd417ad58a40d4fa69c536652e8d89ea6b56e2d709ab71342cc2",
  ]
}

provider "registry.terraform.io/hashicorp/kubernetes" {
  version = "2.0.3"
  hashes = [
    "h1:u3QLP3ca1ZYtdx4t9LrNLzAA3HY/GAT06hznb2pqNC4=",
    "zh:3847733636ed2aca8694227ee6936fecc6cec9573818ecf64acc2a01a4fb3ae4",
    "zh:44d3e88227174d2e51e0273e732e54bb5f5d8150bbf1adecd2be198c857a0264",
    "zh:7c147baeac90ddc71b8c106409dc2e06e6fe04448a0ae36c7ef919b8b27c8d5f",
    "zh:832ebd1eec844fdc00ecbc5e694ecf532d0c7a94f2a1697ea0c7bc159696d529",
    "zh:920c8f64925d346a0621aadede6a4ac1bbe650f16c2074a0a6b5eeb7a5c35cf7",
    "zh:a26906cd3aeb28f402972154dbb3ae1ac6d739ce3e4b00906c9d4b1e263d5a56",
    "zh:a7b3cbd06684f843f700ee9281c583cc593e00e3acc2c0a34f6dad4e35a1ec60",
    "zh:b43c05ade832995a09c40a1c2f080586f38de1edc8c46be2e6f90b96c58a2482",
    "zh:cba2a979889e79ffe48bb5ea7e63f88c344ae2cd7a341afb77b3c8d417658d6c",
    "zh:e00b2ec7357a58a435a69146da3c256178caab83a6574969d4660b69996c7955",
    "zh:f95f179f68aed39b8d73d7914176baf74509d74f90a1e3a600a8eb4461f8f0a3",
  ]
}

provider "registry.terraform.io/sullivtr/graphql" {
  version = "1.4.5"
  hashes = [
    "h1:ntt3Kh5ME6xCqTvUeXux83uZXV48w7Lj4xvn4Eu5GEo=",
    "zh:03c77ae59162a4cb3a858a42beba0355eef0be3ab27b4583b07ae03315aa3cfa",
    "zh:3dd7d799220f0e69cce9a5d4faa8d90bca459dcb612f61c38857aaad1ce84125",
    "zh:422c0b80a72d6bd2b63aa33a4a68aeba5a81849a663eeb06392ba8e139bdb7ff",
    "zh:49c6162e925d97c1e5afcb28b858215c894dc403a787ed8720092bd9982df7a5",
    "zh:60d3a2365e758eda12a3a7ed4265a8de6c2c0438fd085298b906294ee4e64aea",
    "zh:71fc0a2163be8c03b36a9dac11f5234379324f70f32eba025bc2254b9d1091d5",
    "zh:88cf95cb2f367fc6eb624e34e24c1c731881436557686eeb921057ff03cb6996",
    "zh:947d093408e7a5b7b3790ca5f638fcba3d15f1a8affb323c6efa3fdf25d31700",
    "zh:a5f5b60c5831771ad0958e375f33435a9809dd87a5fa821ba92707ca8d312e4e",
    "zh:fc122f55ffb1b4490a9c936995c31f274532fc945add2a8303f115c5cd08ae6d",
    "zh:fe637e069f6d1e6558e8b2c94233d2d347c3bb4f91df3116fac43a2d6fa85499",
  ]
}
