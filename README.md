# Terraform for Kubernetes Cluster on Google Cloud using GitLab Kubernetes Agent

## GitOps Demo Group
See [Global Readme file](https://gitlab.com/gitops-demo/readme/-/blob/master/README.md) for the full details.

For this project a few variables need to be set. You can add them into `terraform.tfvars` to easily import.

**Example `terraform.tfvars`**
```
GITLAB_TOKEN = "zzzPersonalAccessTokenzzz"
project_path = "gitops-demo/infra/kas-gcp"
agent_name   = "gke-agent"
```

**Files in this project:**
```
.
├── LICENSE
├── README.md
├── backend.tf
├── gitlab-agent.tf  # Used to register a gitlab cluster agent and obtain secret
├── gke.tf           # Creates cluster on GCP
├── kas-agent.tf     # Creates KAS Deployment on GKE Cluster
└── versions.tf
```
