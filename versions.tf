terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
    }
    kubernetes = {
      source = "hashicorp/kubernetes"
    }
    graphql = {
      source = "sullivtr/graphql"
    }
  }
  required_version = "~> 0.14.0"
}
